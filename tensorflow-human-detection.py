# Code adapted from Tensorflow Object Detection Framework
# https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb
# Tensorflow Object Detection Detector

import numpy as np
import tensorflow as tf
import cv2
import time
import csv
import glob
from matplotlib import pyplot as plt

#Centroid of the bouding box
class BOXATTRIB:
    centroid =[0]*2
    height   = 0
    width    = 0

filteredBoxAttrib = BOXATTRIB()    
#Calibration error
calibration_bias = 10

class DetectorAPI:
    def __init__(self, path_to_ckpt):
        self.path_to_ckpt = path_to_ckpt

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self.default_graph = self.detection_graph.as_default()
        self.sess = tf.Session(graph=self.detection_graph)

        # Definite input and output Tensors for detection_graph
        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

    def processFrame(self, image):
        # Expand dimensions since the trained_model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image, axis=0)
        # Actual detection.
        start_time = time.time()
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})
        end_time = time.time()

        print("Elapsed Time:", end_time-start_time)

        im_height, im_width,_ = image.shape
        boxes_list = [None for i in range(boxes.shape[1])]
        for i in range(boxes.shape[1]):
            boxes_list[i] = (int(boxes[0,i,0] * im_height),
                        int(boxes[0,i,1]*im_width),
                        int(boxes[0,i,2] * im_height),
                        int(boxes[0,i,3]*im_width))

        return boxes_list, scores[0].tolist(), [int(x) for x in classes[0].tolist()], int(num[0])

    def close(self):
        self.sess.close()
        self.default_graph.close()


def compareHistogram(image1,image2):
    image1_hsv = cv2.cvtColor(image1, cv2.COLOR_BGR2HSV)
    image2_hsv = cv2.cvtColor(image2, cv2.COLOR_BGR2HSV)
    hbins = 180
    sbins = 255
    hrange = [0,180]
    srange = [0,256]
    ranges = hrange+srange  # ranges = [0,179,0,255]

    histImage1 = cv2.calcHist(image1_hsv,[0,1],None,[180,256],ranges)
    cv2.normalize(histImage1,histImage1,0,1,cv2.NORM_MINMAX)

    histImage2 = cv2.calcHist(image2_hsv,[0,1],None,[180,256],ranges)
    cv2.normalize(histImage2,histImage2,0,1,cv2.NORM_MINMAX)

    return cv2.compareHist(histImage1,histImage2,CV_COMP_CHISQR)

def calculateHistogramHSV(image,lines):
    # Convert BGR to HSV
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h,s,v = image_hsv[:,:,0],image_hsv[:,:,1],image_hsv[:,:,2]
    
   
    histH = cv2.calcHist([h], [0], None, [180], [0, 180]) 
    histS = cv2.calcHist([s], [0], None, [255], [0, 255])
    histV = cv2.calcHist([v], [0], None, [255], [0, 255])

    #this is to return hist for avg hist calculation
    #histHS = cv2.calcHist(image_hsv,[0,1],None,[180,256],[0,180,0,256])
    #cv2.normalize(histHS,histHS,0,1,cv2.NORM_MINMAX)
    cv2.normalize(histH,histH,0,1,cv2.NORM_MINMAX)
    
    ##plot histogram
    # lines[0].set_ydata(histH)
    # lines[1].set_ydata(histS)
    # lines[2].set_ydata(histV)
    # fig.canvas.draw()


    maxH = max(histH)
    pos  = histH.tolist().index(maxH)
    if not pos > 110 and pos <170:
        dominant_colour = pos
        return dominant_colour,histH    
    return -1,histH     

def calculateHistogramBGR(image):   

    img_ht, img_wd, img_ch = image.shape
    
    #split the image using numpy not opencv split as its too slow
    b_ch,g_ch,r_ch = cv2.split(image)
    
    features = []

    #features = image.reshape((-1,3))
    mean_b = np.mean(b_ch)
    mean_g = np.mean(g_ch)
    mean_r = np.mean(r_ch)
    std_b  = np.std(b_ch)
    std_g  = np.std(g_ch)
    std_r  = np.std(r_ch)
    np.append(features,mean_b)
    np.append(features,mean_g)
    np.append(features,mean_r)
    np.append(features,std_b)
    np.append(features,std_g)
    np.append(features,std_r)
    print(features)

    color = ('b','g','r')
    for ch,col in enumerate(color):
        hist = cv2.calcHist([image],[ch],None,[256],[0,256])
        hist_int = map(int, hist)#to convert to int from float
        print(col)      
        print(max(hist))
        print('Max is at ')
        print(hist.tolist().index(max(hist)))
        #print(hist_int)
        plt.plot(hist,color = col)
        plt.xlim([0,256])
    plt.title('Colour Hitogram')
    plt.show()

def preprocess_kmeans(input,K):
    Z = input.reshape((-1,3))
    # convert to float
    Z = np.float32(Z)
    #define criteria, number of clusters and apply kmean()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 8
    ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)
    
    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    output = res.reshape((input.shape))
    return output

def image_preprocess(input):   
    
    #output = preprocess_kmeans(input,8)
    blurImg = cv2.GaussianBlur(input,(5,5),0)    
    output  = cv2.addWeighted(blurImg,1.5,input,-0.5,0)

    cv2.imshow('Soomthing',output)
    return output

def display_box(img,filtered_box,min_x_dist,min_y_dist):
    imgDisp = img.copy()    
    exitApp = False

    if filtered_box:        
        filteredBoxAttrib.centroid[0] = (filtered_box[2]+filtered_box[0])/2 #y axis -- height
        filteredBoxAttrib.centroid[1] = (filtered_box[3]+filtered_box[1])/2 #x axis -- width     
        filteredBoxAttrib.height = filtered_box[2]-filtered_box[0]
        filteredBoxAttrib.width  = filtered_box[3]-filtered_box[1]

        cv2.rectangle(imgDisp,(filtered_box[1],filtered_box[0]),(filtered_box[3],filtered_box[2]),(255,0,0),2)
        #Draw centroid fo the boundind box
        cv2.circle(imgDisp,(filteredBoxAttrib.centroid[1],filteredBoxAttrib.centroid[0]),4,(0,0,255),-1)
        #Draw center of the image circle
        cv2.circle(imgDisp,(128,128),4,(0,255,0),-1)
        #cv2.putText(imgDisp,"%d,%d" %avg_box_centroid[1] %avg_box_centroid[0],(200,240),font,1,(255,255,0),2,cv2.LINE_AA,True)
        text = "%d,%d" %(min_x_dist,min_y_dist)
        cv2.putText(imgDisp,text,(200,250),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),1,cv2.LINE_AA,False)
        text = "%d" %(filteredBoxAttrib.height)
        cv2.putText(imgDisp,text,(filtered_box[3],filtered_box[2]+5),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,255),1,cv2.LINE_AA,False)
    
    cv2.imshow("Preview", imgDisp)        
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        print('User stopped program!!!')
        exitApp = True
    
    return exitApp    

def process_box(img,boxes, scores, classes, rules, threshold):
    # Visualization of the results of a detection.
    img_ht,img_wd,img_ch = img.shape 
    min_x_dist = img_wd
    min_y_dist = img_ht
    filtered_box =[]
    for i in range(len(boxes)):
        # Class 1 represents human
        if classes[i] == 1 and scores[i] > threshold:
            box = boxes[i]

            #Calcuate distance from center of image to box centroid
            x_dist = abs(img_wd/2-((box[3]+box[1])/2)) 
            y_dist = abs(img_ht/2-((box[2]+box[0])/2))

            #Select the box closest to image center
            if x_dist < min_x_dist and y_dist < min_y_dist:
                min_x_dist = x_dist
                min_y_dist = y_dist
                filtered_box = box
                filteredBoxAttrib.centroid[0] = (filtered_box[2]+filtered_box[0])/2 #y axis -- height
                filteredBoxAttrib.centroid[1] = (filtered_box[3]+filtered_box[1])/2 #x axis -- width     
                filteredBoxAttrib.height = filtered_box[2]-filtered_box[0]
                filteredBoxAttrib.width  = filtered_box[3]-filtered_box[1]  

    if filtered_box:
        #Check if the filtered_box passes the rules        
        rule1 = min_x_dist < (30 + calibration_bias) 
        rule2 = min_y_dist < 30 
        rule3 = filteredBoxAttrib.height > img_ht*(0.6 - 0.2)

        if rules and (rule1 is False or rule2 is False or rule3 is False):
            print("Box did not pass the rules")
            print(rule1,rule2,rule3)
            print(min_x_dist,min_y_dist,filteredBoxAttrib.height)
            filtered_box = []   
    exitApp = display_box(img,filtered_box,min_x_dist,min_y_dist)  # display using OpenCV
        
    return filtered_box, exitApp

if __name__ == "__main__":
    # model_path = '../../../faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb'
    # #model_path = '../../../ssd_mobilenet_v1_coco_11_06_2017/frozen_inference_graph.pb'
    # odapi = DetectorAPI(path_to_ckpt=model_path)
    # threshold = 0.7
    
    ###Hue plot
    fig,(axH,axS,axV) = plt.subplots(3,1)
    plt.subplots_adjust(hspace=.5)
    axH.set_title('Hue')       
    lineH, = axH.plot(np.arange(180),np.zeros((180,)),c='r',lw=1,alpha=0.5)
    axH.set_xlim(0,179)
    axH.set_ylim(0,6000)

    axS.set_title('Saturation')
    lineS, = axS.plot(np.arange(255),np.zeros((255,)),c='g',lw=1,alpha=0.5)
    axS.set_xlim(0,255)
    axS.set_ylim(0,6000)

    axV.set_title('Value')
    lineV, = axV.plot(np.arange(255),np.zeros((255,)),c='b',lw=1,alpha=0.5)
    axV.set_xlim(0,255)
    axV.set_ylim(0,6000)

    lines = [lineH,lineS,lineV]
    plt.ion()
    plt.show()
    ###Hue plot

    filename = '../../../../sensorsuite/2018-07-07_12-49/Track'
    with open('all_candidates_Sat_Game_Session2.csv') as file:
        reader = csv.reader(file)
        count = 0
        
        
        #data = [row for row in reader]
        for row in reader:                  
            #print(row[0])      
            src_hist = [0]*180          
            dominant_colour_src = 0       
            num_images = 0
            for target_img in glob.glob(filename+row[0]+'/*.jpg'):              
                var_img = cv2.imread(target_img)
                dominant_colour,sum_hist = calculateHistogramHSV(var_img,lines)
                
                if dominant_colour > 0:
                    dominant_colour_src += dominant_colour 
                    if num_images>0:
                        dominant_colour_src /=2
                num_images += 1
                #src_hist = np.add(src_hist,sum_hist)
                src_hist = [x+y for x,y in zip(src_hist,sum_hist)]
                
            src_hist = np.asarray(src_hist)   
            
            #src_hist = [float(x) for x in src_hist]#convert to float before division
            
            src_hist = np.divide(src_hist,num_images)
            #src_hist = [x / num_images for x in src_hist]
            
            #print('Dominant colours',dominant_colour_src)
            
            iterrow = iter(row)
            next(iterrow)               
            for i in iterrow:
                trackNo = int(i) # convert to int as csvreader returns string               
                if trackNo > 633 and trackNo < 3594:            

                    num_images = 0
                    dominant_colour_target = 0
                    #tgt_hist = np.zeros(shape=(180)) # avg histogram(H,S) for all the images in the target
                    tgt_hist = [0]*180
                    
                    for img in glob.glob(filename+i+'/*.jpg'):
                        var_img = cv2.imread(img)
                        dominant_colour,sum_hist = calculateHistogramHSV(var_img,lines)
                        if dominant_colour >0:
                            dominant_colour_target += dominant_colour
                            if num_images>0:
                                dominant_colour_target /=2
                        num_images += 1
                        #tgt_hist = np.add(tgt_hist,sum_hist)
                        tgt_hist = [x+y for x,y in zip(tgt_hist,sum_hist)]

                        cv2.imshow("Image",var_img)    
                        key = cv2.waitKey(1)
                        if key & 0xFF == ord('q'):
                            break
                    
                    tgt_hist = np.asarray(tgt_hist)

                    #tgt_hist = [float(x) for x in tgt_hist]#convert to float before division
                    
                    tgt_hist = np.divide(tgt_hist,num_images)
                    #tgt_hist = [x / num_images for x in tgt_hist]                                      
                    
                    score = cv2.compareHist(src_hist,tgt_hist,cv2.HISTCMP_CHISQR)                   
                            
                    #if abs(dominant_colour_src-dominant_colour_target) < 20:
                    #print(row[0],i," are similar")
                    
                    #print('Score of Track', trackNo, 'with Track',int(row[0]))
                    #print(score)

                    if score < 80:
                        print(row[0],i,'are similar')

                        
            if(count>=11):
                break
            count+=1
    # cap = cv2.VideoCapture('../data/Satgame/Track1761/1.jpg',cv2.CAP_IMAGES)   
    
    # while True:
    #     r, img = cap.read()        
    #     #img = cv2.flip(img,0)
    #     if img is None:
    #         print("Done with all the images.")
    #         break        
    #     #img = image_preprocess(img)
    #     boxes, scores, classes, num = odapi.processFrame(img)
    #     filtered_box,exitApp = process_box(img, boxes, scores, classes, False, threshold) 

    #     if exitApp:
    #         break

       
        #img1 = img[box[0]:box[2],box[1]:box[3]] #crop image
                   
        
        
        

       
        


